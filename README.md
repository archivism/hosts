# The Equitable Pornography Domains List (EPDL)

An equitable list, with more Asian sites. Update regularly.

Usage: Place the hosts file on these location, based on your operating system
- Windows: `C:\Windows\System32\drivers\etc\hosts`
- Linux, BSD: `/etc/hosts`
- MacOS: `/private/etc/hosts`
- Android: `/system/etc/hosts`

Feel free to create new issues for:
- Domain(s) you found that contain pornography content(s).
- Domain(s) you found that are false positive(s).